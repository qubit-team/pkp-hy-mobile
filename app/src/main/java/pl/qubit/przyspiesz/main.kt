package pl.qubit.przyspiesz

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_main_dashbord.*
import kotlinx.android.synthetic.main.content_main.*

class main : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var  run_mock = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

        navigation.setOnNavigationItemSelectedListener(switchNavigationButton)

        displayFragment(-1)

    }

    fun switchToDetails(view: View){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.relativeLayout, FragmentDetails())
            .commit()
    }

    fun switchToTrain(view: View){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.relativeLayout, FragmentTrain())
            .commit()
    }

    fun switchToHome(view: View){
        Toast.makeText(this, "Zalogowano!",  Toast.LENGTH_LONG).show()
        navigation.menu.getItem(0).isChecked = true
        run_main_dashbord()
    }

    fun switchToRegister(view: View){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.relativeLayout, FragmentRegister())
            .commit()
    }

    fun switchToLogin(view: View){

        Toast.makeText(this, "Rejestracja przebiegla pomyslnie!",  Toast.LENGTH_LONG).show()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.relativeLayout, FragmentLogin())
            .commit()
    }


    private val switchNavigationButton = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {

            R.id.navigation_login -> {
                navigation.menu.getItem(1).isChecked = true
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentLogin())
                    .commit()
            }
            R.id.navigation_home -> {
                navigation.menu.getItem(0).isChecked = true
                run_main_dashbord()
            }
        }
        false
    }

    private fun run_main_dashbord(){
        val fragment = MainDashbord()
        val args = Bundle()
        args.putBoolean("run_mock", this.run_mock)
        fragment.arguments = args
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.relativeLayout, fragment)
            .commit()

        if (this.run_mock){
            this.run_mock = false
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    fun displayFragment(id: Int){
        when (id) {
            R.id.nav_buy -> {
                run_main_dashbord()
            }
            R.id.nav_tickets -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentTickets())
                    .commit()
            }
            R.id.nav_info -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentDelays())
                    .commit()
            }
            R.id.nav_maps -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentMaps())
                    .commit()
            }
            R.id.nav_partners -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentPartners())
                    .commit()
            }
            R.id.nav_promo -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentHotshot())
                    .commit()
            }
            R.id.nav_wars -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.relativeLayout, FragmentWars())
                    .commit()
            }
            else -> {
                run_main_dashbord()
            }
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        displayFragment(item.itemId)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
