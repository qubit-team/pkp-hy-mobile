package pl.qubit.przyspiesz

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.support.v4.app.NotificationCompat

class NotificationSpeedUp {

    var notification_departure_chanel_id = "Departure_Notification_ID"
    var notification_arrival_chanel_id = "Arrival_Notification_ID"

    fun createNotificationChanel(notification_manager: NotificationManager?) {
        if (notification_manager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notification_arrival_chanel = NotificationChannel(
                    notification_arrival_chanel_id,
                    "Notification_Arrival",
                    NotificationManager.IMPORTANCE_HIGH
                )
                notification_arrival_chanel.description = "Desc!"

                notification_manager.createNotificationChannel(notification_arrival_chanel)

                val notification_departure_chanel = NotificationChannel(
                    notification_departure_chanel_id,
                    "Notification_Departure",
                    NotificationManager.IMPORTANCE_HIGH
                )
                notification_arrival_chanel.description = "Desc!"

                notification_manager.createNotificationChannel(notification_departure_chanel)
            }
        }
    }

    fun notifyToChanel(notification_manager: NotificationManager?,
                         builder:NotificationCompat.Builder,
                         message: String){
        if (notification_manager != null) {
            val notification = builder
                .setSmallIcon(R.mipmap.pkp_ic_with_text)
                .setContentTitle("SpeedUp")
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE).build()

            notification_manager.notify(1, notification)
        }
    }
}