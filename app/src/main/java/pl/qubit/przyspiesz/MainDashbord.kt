package pl.qubit.przyspiesz

import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class MainDashbord(): Fragment() {

    var notification_speed_up = NotificationSpeedUp()
    private var run_mock = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val run_mock = arguments!!.get("run_mock") as Boolean
            if (run_mock) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                notification_speed_up.createNotificationChanel(getNotificationManager())

                var mock_notification = Thread(Mock())
                mock_notification.start()
                this.run_mock  = false
            }
        }
        return inflater.inflate(R.layout.fragment_main_dashbord, null)
    }

    private fun getNotificationBuilder(chanel_id: String): NotificationCompat.Builder {
        return NotificationCompat.Builder(this.context!!, chanel_id)
    }

    private inner class MessageHandler : Handler() {

        override fun handleMessage(msg: Message?) {
            val state = msg!!.data.get("state").toString()
            val message  =  msg!!.data.get("message").toString()
            if (state.equals("arrival")) {
                notification_speed_up.notifyToChanel(
                    getNotificationManager(),
                    getNotificationBuilder(notification_speed_up.notification_arrival_chanel_id),
                    message
                )
            }  else {
                notification_speed_up.notifyToChanel(
                    getNotificationManager(),
                    getNotificationBuilder(notification_speed_up.notification_departure_chanel_id),
                    message
                )
            }
            super.handleMessage(msg)
        }
    }

    private fun getNotificationManager(): NotificationManager?{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getSystemService(this.context!!, NotificationManager::class.java)
        } else {
            return null
        }
    }

    private inner class Mock : Runnable{

        var messageHandler = MessageHandler()

        override fun run(){

            Thread.sleep(1000)
            var bundle = Bundle()
            bundle.putString("state", "arrival")
            bundle.putString("message", "Twoj pociag dojedzie za 15 min")

            var notification = Message()
            notification.data = bundle

            messageHandler.sendMessage(notification)

            Thread.sleep(1000)

            bundle = Bundle()
            bundle.putString("state", "departure")
            bundle.putString("message", "Twoj pociag wyruszy za 1h")

            notification = Message()
            notification.data = bundle

            messageHandler.sendMessage(notification)
        }
    }
}