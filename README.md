# Przyspiesz / SpeedUp
Have you ever wondered what travelling would be like if you had all relevant information in one place? If you could easily manage your train tickets, find your way through platforms and if you got a message, once you're close to your destination station?
Now you have everything you need here and now, in your phone!

### Main features
* Ticket sales system, for fast ticket reservation
* History of your tickets
* Train stations maps that will guide you through the platforms
* Seat reservation, for maximum comfort
* Always fresh information about delays
* Your station notification, so you don't oversleep your destination
* WARS menu if you're hungry
* Latest events and discounts
* Find a hotel or order taxi from one place
* Integration with social networks

### Technology
The application has been created using the most cutting edge technology - Android 9.0 and Kotlin.
We provide train ticket integration for smartwatches, and the application can communicate with any API.
Modular design allows easy modifications and speeds up new feature development.
The application in its crurrent state serves as proof of concept and does not implement data gathering and business logic.

### How to run
To just run the application, just install the `*.apk` file on your phone. You need to have `Allow installing from untrusted sources` option enabled. This should work for any Android version higher than 4.0, but you will get the best results when using Android 9.0.

If you want to modify the application, you will need the Android Studio installed. Project setup is as easy as just opening the directory with the Android Studio and waiting a little but for IDE to update and download all necessary packages.

